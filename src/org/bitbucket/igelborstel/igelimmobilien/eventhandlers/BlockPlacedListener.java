package org.bitbucket.igelborstel.igelimmobilien.eventhandlers;


import org.bitbucket.igelborstel.igelimmobilien.main.IgelImmobilien;
import org.bitbucket.igelborstel.igelimmobilien.main.ImmobilienRegionManager;
import org.bitbucket.igelborstel.igelimmobilien.main.ImmobilienWorldBuilder;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlacedListener implements Listener 
{
	private IgelImmobilien main;
	private final int radius = 8;
	public BlockPlacedListener(IgelImmobilien m)
	{
		this.main = m;
	}

	@EventHandler
	public void onBlockPlaced(BlockPlaceEvent ev)
	{
		if(ev.getBlockPlaced().getType() == Material.getMaterial(this.main.getConfig().getString("create.material").toUpperCase()))	
		{
			//Chest chest = (Chest) ev.getBlockPlaced().getState().getData();
			if(ev.getPlayer().hasPermission("igelimmobilien.create.plot"))
			{
				if(this.main.getDatabaseControl().getPlotCount(ev.getPlayer()) < this.main.getDatabaseControl().getMaximumPlotCount(ev.getPlayer()))
				{
					if(ev.getBlock().getLocation().getWorld().getName().equalsIgnoreCase(this.main.getConfig().getString("create.world")))
					{
						String name = ImmobilienRegionManager.generateName(ev.getPlayer(), ev.getPlayer().getLocation());
						if(this.main.getRegionManger().createRegion(radius*2, ev.getPlayer(), ev.getPlayer().getLocation(), name))
						{
							this.main.getDatabaseControl().newPlot(ev.getBlock().getLocation(), radius, name, false, false, false, true);
							this.main.getDatabaseControl().incrementPlotCount(ev.getPlayer());
							ev.getPlayer().sendMessage("Grundstück erfolgreich gesetzt. Name des Grundstücks: " + name);
							ImmobilienWorldBuilder.generate(ev.getBlockPlaced().getLocation(), radius*2, radius, ev.getBlockPlaced().getWorld());
							this.main.getDatabaseControl().setOwner(this.main.getDatabaseControl().getPlotId(name), ev.getPlayer());
						}
					}
					else
					{
						ev.getPlayer().sendMessage("In dieser Welt kannst du keine Grundstücke erzeugen!");
					}
					
				}
				else
				{
					ev.getPlayer().sendMessage("Grundstückerstellung nicht möglich. Besitzt du bereits eins?");
				}
			}
		}
	}
}
