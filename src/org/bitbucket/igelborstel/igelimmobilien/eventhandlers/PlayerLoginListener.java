package org.bitbucket.igelborstel.igelimmobilien.eventhandlers;

import org.bitbucket.igelborstel.igelimmobilien.main.IgelImmobilien;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PlayerLoginListener implements Listener
{
	private IgelImmobilien main;
	public PlayerLoginListener(IgelImmobilien m) 
	{
		this.main = m;
	}
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent ev)
	{
		this.main.database.doUpdate("INSERT INTO `igelimmobilien_player` (`uuid`, `plotcount`, `maximumplotcount`) VALUES (\'" + ev.getPlayer().getUniqueId().toString()  
									+ "\', \'" + 0 + "\', \'" + this.main.getConfig().getInt("create.maximumPlots") + "\') ON DUPLICATE KEY UPDATE uuid=uuid");
	}

}
