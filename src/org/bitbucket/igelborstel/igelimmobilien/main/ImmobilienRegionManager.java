package org.bitbucket.igelborstel.igelimmobilien.main;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.evilmidget38.UUIDFetcher;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.RegionGroup;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class ImmobilienRegionManager 
{
	IgelImmobilien main;
	
	ImmobilienRegionManager(IgelImmobilien m)
	{
		this.main = m;
	}
	
	private WorldGuardPlugin getWorldGuardPlugin()
	{
		Plugin plugin = this.main.getServer().getPluginManager().getPlugin("WorldGuard");
        if(plugin == null || !(plugin instanceof WorldGuardPlugin))
            return null;
        else
            return (WorldGuardPlugin)plugin;
	}
	
	public boolean createRegion(int length, Player owner, Location centralPoint, String regionname)
	{
		String world = centralPoint.getWorld().getName();
		if(length % 2 != 0)
		{
			owner.sendMessage("Die Gr��e f�r das Grundst�ck ist falsch angegeben!");
			return false;
		}
		Location firstcorner, secondcorner;
		firstcorner = new Location(this.main.getServer().getWorld(world), centralPoint.getX() + (length / 2), centralPoint.getY() + (length / 2), centralPoint.getZ() + (length / 2));
		secondcorner = new Location(this.main.getServer().getWorld(world), centralPoint.getX() - (length / 2), centralPoint.getY() - (length / 2), centralPoint.getZ() - (length / 2));
		BlockVector bvfirst = new BlockVector(firstcorner.getX(), 0, firstcorner.getZ());
		BlockVector bvseconde = new BlockVector(secondcorner.getX(), 255, secondcorner.getZ());
		ProtectedRegion region = new ProtectedCuboidRegion(regionname, bvfirst, bvseconde);
		region.setFlag(DefaultFlag.PASSTHROUGH, StateFlag.State.DENY);
		region.setFlag(DefaultFlag.CHEST_ACCESS, StateFlag.State.ALLOW);
		region.setFlag(DefaultFlag.CHEST_ACCESS.getRegionGroupFlag(), RegionGroup.MEMBERS);
		region.setFlag(DefaultFlag.USE, StateFlag.State.ALLOW);
		region.setFlag(DefaultFlag.USE.getRegionGroupFlag(), RegionGroup.MEMBERS);
		//region.setFlag(DefaultFlag.GREET_MESSAGE, "Region " + region.getId());
		RegionContainer container = getWorldGuardPlugin().getRegionContainer();
		RegionManager regions = container.get(firstcorner.getWorld());
		if(!regions.overlapsUnownedRegion(region, getWorldGuardPlugin().wrapPlayer(owner)))
		{
			   DefaultDomain owners = region.getOwners();
			   owners.addPlayer(owner.getName());
        	   regions.addRegion(region);
        	   try {
			   regions.save();
        	   } catch (StorageException e) {
					e.printStackTrace();
					return false;
			   }
		}
		else
		{
			owner.sendMessage("Deine Region �berschneidet sich mit einer bereits vorhandenen.");
		}
		return true;
	}
	
	public boolean addMember(Player owner, String membername, int plotid)
	{
		UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(membername));
		Map<String, UUID> response = null;
		try 
		{
		response = fetcher.call();
		} 
		catch (Exception e) 
		{
			this.main.getLogger().warning("Exception while running UUIDFetcher");
			e.printStackTrace();
		}
		if(response != null && (response.get(membername) != null))
		{
			RegionContainer container = getWorldGuardPlugin().getRegionContainer();
			RegionManager regions = container.get(owner.getWorld());
			if(regions != null)
			{
				ProtectedRegion region = regions.getRegion(this.main.getDatabaseControl().getPlotName(plotid));
				if(region != null)
				{
					DefaultDomain members = region.getMembers();
					members.addPlayer(response.get(membername));
					this.main.getDatabaseControl().setMember(plotid, response.get(membername).toString());
					try 
					{
						regions.save();
					} 
					catch (StorageException e) 
					{
						System.err.println("[Igelimmobilien] Could not save Region!");
						e.printStackTrace();
						return false;
					}
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
			
		}
		else
		{
			owner.sendMessage("Den Spieler " + membername + " gibt es nicht");
			return false;
		}
	}
	
	public boolean removeMember(Player owner, String membername, int plotid)
	{
		UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(membername));
		Map<String, UUID> response = null;
		try 
		{
		response = fetcher.call();
		} 
		catch (Exception e) 
		{
			this.main.getLogger().warning("Exception while running UUIDFetcher");
			e.printStackTrace();
		}
		if(response != null && (response.get(membername) != null))
		{
			RegionContainer container = getWorldGuardPlugin().getRegionContainer();
			RegionManager regions = container.get(owner.getWorld());
			if(regions != null)
			{
				ProtectedRegion region = regions.getRegion(this.main.getDatabaseControl().getPlotName(plotid));
				if(region != null)
				{
					DefaultDomain members = region.getMembers();	
					members.removePlayer(response.get(membername));
					this.main.getDatabaseControl().removeMember(plotid, response.get(membername).toString());
					try 
					{
						regions.save();
					} 
					catch (StorageException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
			
		}
		else
		{
			owner.sendMessage("Den Spieler " + membername + " gibt es nicht");
			return false;
		}
	}
	
	public String getPlotNameAt(Location loc)
	{
		ApplicableRegionSet regions = this.getWorldGuardPlugin().getRegionManager(loc.getWorld()).getApplicableRegions(loc);
		for(ProtectedRegion region : regions)
		{
			if(region.getId().contains("_plot"))
			{
				return region.getId();
			}
		}
		return "";
	}
	
	public static String generateName(Player player, Location loc)
	{
		String temp = "";
		temp = player.getName() + '_' + loc.getBlockX() + "_plot";
		return temp;
	}
	
}
