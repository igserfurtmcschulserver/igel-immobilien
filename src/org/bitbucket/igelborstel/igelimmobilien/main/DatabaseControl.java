package org.bitbucket.igelborstel.igelimmobilien.main;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class DatabaseControl 
{
	IgelImmobilien main;
	
	DatabaseControl(IgelImmobilien m)
	{
		this.main = m;
	}
	
	public void newPlot(Location loc, int radius, String name, boolean flagPassthrough, boolean flagChestAccess, boolean flagUse, boolean flagPVP)
	{
		this.main.database.doUpdate("INSERT INTO `igelimmobilien_plot` (`name`, `world`, `px0`, `pz0`, `px1`, `pz1`, `flagPassthrough`, `flagChestAccess`, `flagUse`, `flagPVP`)"
									 + "VALUES (\'" + name + "\', \'" + loc.getWorld().getName() + "\', \'" + (loc.getBlockX() - radius) + "\', \'" + (loc.getBlockZ() - radius) 
									 + "\', \'" + (loc.getBlockX() + radius) + "\', \'" + (loc.getBlockZ() + radius) + "\', " + flagPassthrough + ", " + flagChestAccess + ", "
									 + flagUse + ", " + flagPVP + ")");
	}
	public void incrementPlotCount(Player player)
	{
		this.main.database.doUpdate("UPDATE `igelimmobilien_player` SET `plotcount` = `plotcount` + 1 WHERE `uuid`= \'" + player.getUniqueId().toString() + '\'');
	}
	
	public void decrementPlotCount(Player player)
	{
		this.main.database.doUpdate("UPDATE `igelimmobilien_player` SET `plotcount` = `plotcount` - 1 WHERE `uuid`= \'" + player.getUniqueId().toString() + '\'');
	}
	
	public int getPlotCount(Player player)
	{
		ResultSet results = this.main.database.doQuery("SELECT `plotcount` FROM `igelimmobilien_player` WHERE `uuid`= \'" + player.getUniqueId().toString() + '\'');
		int plots = 0;
		try 
		{
			while(results.next())
			{
				plots = results.getInt("plotcount");
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return plots;
	}
	
	public int getMaximumPlotCount(Player player)
	{
		ResultSet results = this.main.database.doQuery("SELECT `maximumplotcount` FROM `igelimmobilien_player` WHERE `uuid`= \'" + player.getUniqueId().toString() + '\'');
		int maxplots = this.main.getConfig().getInt("create.maximumPlots");
		try 
		{
			while(results.next())
			{
				maxplots = results.getInt("maximumplotcount");
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return maxplots;
	}

	public void incrementMaximumPlotCount(Player player)
	{
		this.main.database.doUpdate("UPDATE `igelimmobilien_player` SET `maximumplotcount` = `maximumplotcount` + 1 WHERE `uuid`= \'" + player.getUniqueId().toString() + '\'');
	}
	
	public void decrementMaximumPlotCount(Player player)
	{
		this.main.database.doUpdate("UPDATE `igelimmobilien_player` SET `maximumplotcount` = `maximumplotcount` - 1 WHERE `uuid`= \'" + player.getUniqueId().toString() + '\'');
	}
	
	public int getPlotId(String name) 
	{
		ResultSet results = this.main.database.doQuery("SELECT `id` FROM `igelimmobilien_plot` WHERE `name`= \'" + name + '\'');
		int plotid = 0;
		try 
		{
			while(results.next())
			{
				plotid = results.getInt("id");
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return plotid;
	}
	
	public String getPlotName(int id)
	{
		ResultSet results = this.main.database.doQuery("SELECT `name` FROM `igelimmobilien_plot` WHERE `id`= \'" + id + '\'');
		String plotname = "";
		try 
		{
			while(results.next())
			{
				plotname = results.getString("name");
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return plotname;
	}
	public void setOwner(int plotid, Player owner)
	{
		this.main.database.doUpdate("INSERT INTO `igelimmobilien_member` (`id`, `uuid`, `owner`, `member`) VALUES (" + plotid + ", \'" + owner.getUniqueId() + "\', TRUE, FALSE)");
	}
	
	public void setMember(int plotid, Player member)
	{
		this.main.database.doUpdate("INSERT INTO `igelimmobilien_member` (`id`, `uuid`, `owner`, `member`) VALUES (" + plotid + ", \'" + member.getUniqueId() + "\', FALSE, TRUE)");
	}
	
	public void removeOwner(int plotid, Player owner)
	{
		this.main.database.doUpdate("DELETE FROM `igelimmobilien_member` WHERE `id`=" + plotid + " AND `uuid`=\'" + owner.getUniqueId() + "\'");
	}
	
	public void removeMember(int plotid, Player member)
	{
		this.main.database.doUpdate("DELETE FROM `igelimmobilien_member` WHERE `id`=" + plotid + " AND `uuid`=\'" + member.getUniqueId() + "\'");
	}
	
	public void setOwner(int plotid, String owneruuid)
	{
		this.main.database.doUpdate("INSERT INTO `igelimmobilien_member` (`id`, `uuid`, `owner`, `member`) VALUES (" + plotid + ", \'" + owneruuid + "\', TRUE, FALSE)");
	}
	
	public void setMember(int plotid, String memberuuid)
	{
		this.main.database.doUpdate("INSERT INTO `igelimmobilien_member` (`id`, `uuid`, `owner`, `member`) VALUES (" + plotid + ", \'" + memberuuid + "\', FALSE, TRUE)");
	}
	
	public void removeOwner(int plotid, String owneruuid)
	{
		this.main.database.doUpdate("DELETE FROM `igelimmobilien_member` WHERE `id`=" + plotid + " AND `uuid`=\'" + owneruuid + "\'");
	}
	
	public void removeMember(int plotid, String memberuuid)
	{
		this.main.database.doUpdate("DELETE FROM `igelimmobilien_member` WHERE `id`=" + plotid + " AND `uuid`=\'" + memberuuid + "\'");
	}
}

