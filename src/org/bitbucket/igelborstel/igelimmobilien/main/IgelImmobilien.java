package org.bitbucket.igelborstel.igelimmobilien.main;

import org.bitbucket.igelborstel.igelcore.database.MySql;
import org.bitbucket.igelborstel.igelimmobilien.commandhandler.MemberControlCommands;
import org.bitbucket.igelborstel.igelimmobilien.eventhandlers.BlockPlacedListener;
import org.bitbucket.igelborstel.igelimmobilien.eventhandlers.PlayerLoginListener;
import org.bukkit.plugin.java.JavaPlugin;

public class IgelImmobilien extends JavaPlugin 
{

	private ImmobilienRegionManager rm;
	public MySql database;
	private DatabaseControl dbcontrol;
	
	@Override
	public void onEnable()
	{
		loadConfig();
		rm = new ImmobilienRegionManager(this);
		dbcontrol = new DatabaseControl(this);
		this.getServer().getPluginManager().registerEvents(new BlockPlacedListener(this), this);
		this.getServer().getPluginManager().registerEvents(new PlayerLoginListener(this), this);
		this.getCommand("membercontrol").setExecutor(new MemberControlCommands(this));
		
		database = new MySql(this.getDescription().getName());
		if(database.connect())
		{
			createTables();
		}
		
	}
	
	@Override
	public void onDisable()
	{
		if(database.isConnected())
		{
			database.close();
		}
	}
	
	public void loadConfig()
	{
		this.reloadConfig();
		this.getConfig().options().header("Konfigurationsdatei f�r Igel-Immobilien");
		this.getConfig().addDefault("create.material", "chest");
		this.getConfig().addDefault("create.maximumPlots", 1);
		this.getConfig().addDefault("create.world", "World");
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
	}
	
	
	public ImmobilienRegionManager getRegionManger()
	{
		return rm;
	}
	
	public DatabaseControl getDatabaseControl()
	{
		return dbcontrol;
	}
	
	private void createTables()
	{
		if(database != null)
		{
			this.database.doUpdate("CREATE TABLE IF NOT EXISTS `igelimmobilien_player` (uuid VARCHAR(36) NOT NULL PRIMARY KEY, plotcount INT, maximumplotcount INT)");
			this.database.doUpdate("CREATE TABLE IF NOT EXISTS `igelimmobilien_plot` (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(36), world VARCHAR(16), px0 INT, pz0 INT, px1 INT, pz1 INT, flagPassthrough BOOLEAN, flagChestAccess BOOLEAN, flagUse BOOLEAN, flagPVP BOOLEAN)");
			this.database.doUpdate("CREATE TABLE IF NOT EXISTS `igelimmobilien_member` (nr INT NOT NULL PRIMARY KEY AUTO_INCREMENT, id INT NOT NULL, uuid VARCHAR(36), owner BOOLEAN, member BOOLEAN)");
		}
		else
		{
			System.err.println("[IgelImmobilien] Nullpointer");
		}
	}
}
