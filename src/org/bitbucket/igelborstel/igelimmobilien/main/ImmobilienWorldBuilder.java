package org.bitbucket.igelborstel.igelimmobilien.main;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;


public class ImmobilienWorldBuilder 
{
	public ImmobilienWorldBuilder(IgelImmobilien plugin)
	{
	    this.plugin = plugin;
	}

	    static public void generate(Location middlePoint, Integer size, Integer half_size, World world)
	    {
	        int x = middlePoint.getBlockX();
	        int y = middlePoint.getBlockY();
	        int z = middlePoint.getBlockZ();
	        int height = 1;
	        Material material = Material.FENCE;
	        Block blockToChange = new Location(world, x, y, z).getBlock();
	        for(int b = 0; b < size.intValue() + 2; b++)
	        {
	            for(blockToChange = world.getBlockAt(x - half_size.intValue() - 1, y, (z - half_size.intValue() - 1) + b); blockToChange.getType() != Material.AIR;)
	            {
	                blockToChange = world.getBlockAt(x - half_size.intValue() - 1, y + height, (z - half_size.intValue() - 1) + b);
	                height++;
	            }

	            height = 1;
	            blockToChange.setType(material);
	            for(blockToChange = world.getBlockAt(x + half_size.intValue() + 1, y, (z - half_size.intValue() - 1) + b); blockToChange.getType() != Material.AIR;)
	            {
	                blockToChange = world.getBlockAt(x + half_size.intValue() + 1, y + height, (z - half_size.intValue() - 1) + b);
	                height++;
	            }

	            height = 1;
	            blockToChange.setType(material);
	        }

	        for(int b = 0; b < size.intValue(); b++)
	        {
	            for(blockToChange = world.getBlockAt((x - half_size.intValue()) + b, y, z + half_size.intValue() + 1); blockToChange.getType() != Material.AIR;)
	            {
	                blockToChange = world.getBlockAt((x - half_size.intValue()) + b, y + height, z + half_size.intValue() + 1);
	                height++;
	            }

	            height = 1;
	            blockToChange.setType(material);
	            for(blockToChange = world.getBlockAt((x - half_size.intValue()) + b, y, z - half_size.intValue() - 1); blockToChange.getType() != Material.AIR;)
	            {
	                blockToChange = world.getBlockAt((x - half_size.intValue()) + b, y + height, z - half_size.intValue() - 1);
	                height++;
	            }

	            height = 1;
	            blockToChange.setType(material);
	        }

	    }
	    private final IgelImmobilien plugin;
}


