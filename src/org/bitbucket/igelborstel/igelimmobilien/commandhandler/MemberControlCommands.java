package org.bitbucket.igelborstel.igelimmobilien.commandhandler;

import org.bitbucket.igelborstel.igelcore.commandHandlers.PlayerCommand;
import org.bitbucket.igelborstel.igelimmobilien.main.IgelImmobilien;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class MemberControlCommands extends PlayerCommand 
{
	private IgelImmobilien main;
	
	public MemberControlCommands(IgelImmobilien m)
	{
		this.main = m;
	}

	@Override
	public boolean command(Player player, Command cmd, String[] args) 
	{
		if(cmd.getName().equalsIgnoreCase("membercontrol"))
		{
			if((args.length == 2))
			{
				if(args[0].equalsIgnoreCase("add"))
				{
					if(player.hasPermission("igelimmobilien.membercontrol.add"))
					{
						
						if(this.main.getRegionManger().addMember(player, args[1], this.main.getDatabaseControl().getPlotId(this.main.getRegionManger().getPlotNameAt(player.getLocation()))))
						{
							player.sendMessage("Spieler " + args[1] + " im Grundstück " + this.main.getRegionManger().getPlotNameAt(player.getLocation()) + "erfolgreich als Mitbewohner in dem Grundstück eingetragen!");
							return true;
						}
						else
						{
							player.sendMessage("Konnte den Spieler nicht hinzufügen. Befindest du dich nicht in deinem Grundstück?");
							return true;
						}
					}
					else
					{
						return false;
					}
				}
				else if(args[0].equalsIgnoreCase("remove"))
				{
					if(player.hasPermission("igelimmobilien.membercontrol.remove"))
					{
						if(this.main.getRegionManger().removeMember(player, args[1], this.main.getDatabaseControl().getPlotId(this.main.getRegionManger().getPlotNameAt(player.getLocation()))))
						{
							player.sendMessage("Spieler " + args[1] + " im Grundstück " + this.main.getRegionManger().getPlotNameAt(player.getLocation()) + "erfolgreich als Mitbewohner in dem Grundstück ausgetragen!");
							return true;
						}
						else
						{
							player.sendMessage("Konnte den Spieler nicht entfernen. Befindest du dich nicht in deinem Grundstück?");
							return true;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
		
			return false;
		}
	}

}
