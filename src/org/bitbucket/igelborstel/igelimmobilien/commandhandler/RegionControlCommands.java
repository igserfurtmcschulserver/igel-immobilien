package org.bitbucket.igelborstel.igelimmobilien.commandhandler;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.bitbucket.igelborstel.igelimmobilien.main.IgelImmobilien;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.evilmidget38.UUIDFetcher;

public class RegionControlCommands implements CommandExecutor
{
	
	private IgelImmobilien main;
	
	public RegionControlCommands(IgelImmobilien m)
	{
		this.main = m;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		if(args.length < 2 && args.length > 2)
		{
			return false;
		}
		else
		{
			if(command.getName().equalsIgnoreCase("regioncontrol"))
			{
				if(args[0].equalsIgnoreCase("add"))
				{
					if(sender.hasPermission("igelimmobilien.regioncontrol.add"))
					{
						UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(args[1]));
						Map<String, UUID> response = null;
						try 
						{
						response = fetcher.call();
						} 
						catch (Exception e) 
						{
							e.printStackTrace();
						}
						if(response != null && (response.get(args[1]) != null))
						{
							this.main.getDatabaseControl().incrementMaximumPlotCount(Bukkit.getPlayer(response.get(args[1])));
							sender.sendMessage("Increment maximum plotcount for player " + args[1]);
						}
					}
					else
					{
						return true;
					}
				}
				else if(args[0].equalsIgnoreCase("remove"))
				{
					if(sender.hasPermission("igelimmobilien.regioncontrol.remove"))
					{
						UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(args[1]));
						Map<String, UUID> response = null;
						try 
						{
						response = fetcher.call();
						} 
						catch (Exception e) 
						{
							e.printStackTrace();
						}
						if(response != null && (response.get(args[1]) != null))
						{
							this.main.getDatabaseControl().decrementMaximumPlotCount(Bukkit.getPlayer(response.get(args[1])));
							sender.sendMessage("Decrement maximum plotcount for player " + args[1]);
						}
					}
					else
					{
						return true;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		// TODO Auto-generated method stub
		return false;
	}

}
